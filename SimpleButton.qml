import QtQuick 2.5

BorderImage {
    id: btnBase

    signal clicked

    property alias text: btnLabel.text

    source: innerMa.pressed ? "/images/img/Blue_Button_Pressed_2.png" : "/images/img/Blue_Button_2.png"
    border { left: 13; top: 13; right: 13; bottom: 13}

    Text {
        id: btnLabel
        anchors.centerIn: parent
        color: "white"
        font.pointSize: 10
    }

    MouseArea {
        id: innerMa
        anchors.fill: parent
        onClicked: btnBase.clicked()
    }
}

