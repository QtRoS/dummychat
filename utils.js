.pragma library

.import "messages.js" as Messages

// So simple way.
var smiles = [
    { "smile" : /;\)/g, "image" : "<img src='/images/img/emoji/D83DDE1C.png'/>" },
    { "smile" : /:\)/g, "image" : "<img src='/images/img/emoji/D83DDE0A.png'/>" },
    { "smile" : /:\(/g, "image" : "<img src='/images/img/emoji/D83DDE14.png'/>" }
]

function preprocessText(text) {
    // Global optimization.
    text = text.replace(/\n/g, "<br>")
    if (!text.match(/:|;|\(|\)/g))
        return text

    for (var i = 0; i < smiles.length; i++) {
        var sm = smiles[i]
        text = text.replace(sm.smile, sm.image)
    }
    return text
}

var messageCounter = 1

function createNewMessage(text) {
    var id = messageCounter++
    var fileRx = /\[.*\]/g
    var isTransfer = text.match(fileRx)
    var type = isTransfer ? "SimpleControl" : "Text"
    var obj = {
        "id" : id,
        "out" : id % 2,
        "date" : new Date(),
        "body" : preprocessText(text),
        "type" : type ? type : "Text",
        "isSelected" : false
    }
    return obj
}

var offset = 0
function loadMoreItems(model) {
    console.log("loadMoreItems", offset)

    var data = Messages.data
    if (data.length <= offset)
        return

    for (var i = 0; i + offset < data.length && i < 30; i++) {
        var d = data[offset + i]
        var obj = {
            "id" : d.id,
            "out" : d.out,
            "date" : new Date(d.date),
            "body" : preprocessText(d.body),
            "type" : !((i+1) % 8) ? "SimpleControl" : "Text",
            "isSelected" : false
        }
        model.append(obj)
        offset++
    }
}
