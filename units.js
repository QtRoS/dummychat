.pragma library

var dip = 1
var os
var baseUnitSize = 4

function gu(value) {
    if (!os)
        os = Qt.platform.os

    if (os === "android" || os === "ios")
        return baseUnitSize * dip * value
    else return baseUnitSize * value
}

// Required for mobile platforms.
function initUnits(dpm) {
    var dpi = dpm * 25.4
    dip = dpi / 160
    dip = Math.round(dip * 10) / 10
}
