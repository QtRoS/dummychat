import QtQuick 2.5

ListView {
    id: mainList

    verticalLayoutDirection: ListView.BottomToTop
    delegate: MessageDelegate {
        onClicked: {
            var o = mainList.model.get(index)
            o.isSelected = !o.isSelected
        }
    }
}
