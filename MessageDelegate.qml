import QtQuick 2.5

import "units.js" as Units

Rectangle {
    id: chatMsgDelegRoot

    property bool isIncoming: !model.out
    property bool isSelected: model.isSelected

    signal clicked(int index)

    width: parent.width
    height: dlgColumn.height + Units.gu(2.5)
    color: "#edf1f5"

    Column {
        id: dlgColumn
        spacing: Units.gu(4)
        width: parent.width
        anchors.verticalCenter: parent.verticalCenter

        BorderImage {
            id: borderImage

            source: isIncoming?
                        (isSelected ? "/images/img/MsgOut_Selected_2.png" : "/images/img/MsgOut_2.png") :
                        (isSelected ? "/images/img/MsgIn_Selected_2.png" : "/images/img/MsgIn_2.png")

            // Texture-dependent.
            border {
                left: isIncoming? 20 : 30
                top: 20
                right: isIncoming? 30 : 20
                bottom: 35
            }

            anchors {
                left: isIncoming? undefined : parent.left
                right: isIncoming? parent.right : undefined
            }
            width: Math.max(content.width + Units.gu(15), Units.gu(21))
            height: content.height + Units.gu(9)

            MouseArea {
                id: msgDelegateMa
                anchors.fill: parent
                onClicked: chatMsgDelegRoot.clicked(model.index)
            }

            Loader {
                id: content
                sourceComponent: model.type === "Text" ? textComponent : controlComponent
                anchors {
                    left: isIncoming? undefined : parent.left
                    right: isIncoming? parent.right : undefined
                    leftMargin: Units.gu(10)
                    rightMargin: Units.gu(10)
                    top: parent.top
                    topMargin: Units.gu(4)
                }
            }

            Text {
                text: model.date.toTimeString()
                font.pointSize: 8
                font.italic: true;
                color: "lightgrey"
                anchors {
                    left: isIncoming? undefined : parent.left
                    right: isIncoming? parent.right : undefined
                    rightMargin: Units.gu(7.5)
                    leftMargin: Units.gu(7.5)
                    bottom: parent.bottom
                    bottomMargin: Units.gu(1)
                }
            }
        } // BorderImage
    } // Column

    // TODO To separate files.
    Component {
        id: textComponent

        Rectangle {
            id: textComponentRoot

            color: "#00000000"
            width: msgText.paintedWidth
            height: msgText.height

            Text {
                id: msgText
                font.pointSize: 10
                textFormat: Text.RichText
                wrapMode: Text.WrapAtWordBoundaryOrAnywhere
                width: chatMsgDelegRoot.width * 0.7
                text: model.body
                color: isSelected? "white" : "black"
            }
        }
    } // Component

    Component {
        id: controlComponent

        Rectangle {
            id: textComponentRoot

            color: "#00000000"
            width: innerColumn.width
            height: innerColumn.height

            Column {
                id: innerColumn
                spacing: Units.gu(1)

                Text {
                    id: fileNameText
                    font.pointSize: 10
                    wrapMode: Text.WrapAtWordBoundaryOrAnywhere
                    width: chatMsgDelegRoot.width * 0.7
                    elide: Text.ElideRight
                    text: "File transfer: " + model.body
                    color: isSelected? "white" : "black"
                }

                Row {
                    id: innerRow
                    anchors.right: parent.right
                    spacing: Units.gu(1)

                    SimpleButton {
                        id: allowBtn
                        width: Units.gu(15)
                        height: Units.gu(8)
                        text: "Allow"
                    }

                    SimpleButton {
                        id: denyBtn
                        width: Units.gu(15)
                        height: Units.gu(8)
                        text: "Deny"
                    }
                }
            } // Column
        }
    } // Component
}

