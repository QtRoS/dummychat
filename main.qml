import QtQuick 2.5
import QtQuick.Window 2.2

import "utils.js" as Utils
import "messages.js" as Messages
import "units.js" as Units

Window {

    function sendMessage() {
        if (!chatInputEdit.text) // Naive.
            return

        simpleModel.insert(0, Utils.createNewMessage(chatInputEdit.text))

        chatInputEdit.text = ""
        chatComponent.positionViewAtBeginning()
    }

    function loadMore() {
        Utils.loadMoreItems(simpleModel)
    }

    visible: true
    width: Units.gu(80)
    height: Units.gu(120)

    Component.onCompleted: loadMore()

    ChatComponent {
        id: chatComponent
        model: simpleModel
        anchors {
            top: parent.top
            left: parent.left
            right: parent.right
            bottom: bottomBar.top
        }
        onContentYChanged: {
            if ((visibleArea.yPosition < (visibleArea.heightRatio * 3))) {
                loadMore()
            }
        }
    }

    Rectangle{
        id: bottomBar
        height: chatInputEdit.height + Units.gu(4)
        width: parent.width
        anchors.bottom: parent.bottom
        anchors.left: parent.left
        color: "white"

        ScalableTextEdit {
            id: chatInputEdit
            anchors {
                verticalCenter: parent.verticalCenter
                left: parent.left
                leftMargin: Units.gu(1)
                right: sendButton.left
                rightMargin: Units.gu(1)
            }
            onReturnPressed: sendMessage()
        }

        SimpleButton {
            id: sendButton
            width: Units.gu(20)
            height: Units.gu(10)
            text: "Send"

            anchors {
                verticalCenter: parent.verticalCenter
                right: parent.right
                rightMargin: Units.gu(1)
            }
            onClicked: sendMessage()
        }
    } // Rectangle


    ListModel {
        id: simpleModel
    }
}

