import QtQuick 2.5

import "units.js" as Units

Flickable {
    id: flickable

    property alias text: innerTextEdit.text

    signal returnPressed()

    height: Math.max (Units.gu(10), Math.min(innerTextEdit.implicitHeight, Units.gu(35)))
    contentWidth: innerTextEdit.paintedWidth
    contentHeight: innerTextEdit.paintedHeight
    flickableDirection: Flickable.VerticalFlick
    clip: true

    function ensureVisible(r) {
        if (contentX >= r.x)
            contentX = r.x;
        else if (contentX+width <= r.x+r.width)
            contentX = r.x+r.width-width;
        if (contentY >= r.y)
            contentY = r.y;
        else if (contentY+height <= r.y+r.height)
            contentY = r.y+r.height-height;
    }

    TextEdit {
        id: innerTextEdit
        width: flickable.width
        height: flickable.height
        focus: true
        wrapMode: TextEdit.Wrap
        selectByMouse: true
        onCursorRectangleChanged: flickable.ensureVisible(cursorRectangle)
        font.pointSize: 10
        Keys.onPressed: {
            if ((event.key == Qt.Key_Return) && (event.modifiers & Qt.ControlModifier)) {
                flickable.returnPressed()
            }
        }

        Text { text: "Type message here..."; font.pointSize: 10; visible: !innerTextEdit.length; font.italic: true; color: "grey" }
    }
}
